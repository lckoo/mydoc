
** 错误信息 **

参考文章：
http://blog.sina.com.cn/s/blog_6bb63c9e0100s7cb.html
```text
Lock wait timeout exceeded
```


| Field | Type | Null | Key | Default | Extra |说明|
|---|---|---|---|---|---|---|
| trx_id | varchar(18) | NO | | | |#事务ID|
| trx_state | varchar(13) | NO | | | |#事务状态：|
| trx_started | datetime | NO | | 0000-00-00 00:00:00 | |#事务开始时间；|
| trx_requested_lock_id | varchar(81) | YES | | NULL | |#innodb_locks.lock_id|
| trx_wait_started | datetime | YES | | NULL | |#事务开始等待的时间|
| trx_weight | bigint(21) unsigned | NO | | 0 | |#|
| trx_mysql_thread_id | bigint(21) unsigned | NO | | 0 | |#事务线程ID|
| trx_query | varchar(1024) | YES | | NULL | |#具体SQL语句|
| trx_operation_state | varchar(64) | YES | | NULL | |#事务当前操作状态|
| trx_tables_in_use | bigint(21) unsigned | NO | | 0 | |#事务中有多少个表被使用|
| trx_tables_locked | bigint(21) unsigned | NO | | 0 | |#事务拥有多少个锁|
| trx_lock_structs | bigint(21) unsigned | NO | | 0 | |#|
| trx_lock_memory_bytes | bigint(21) unsigned | NO | | 0 | |#事务锁住的内存大小（B）|
| trx_rows_locked | bigint(21) unsigned | NO | | 0 | |#事务锁住的行数|
| trx_rows_modified | bigint(21) unsigned | NO | | 0 | |#事务更改的行数|
| trx_concurrency_tickets | bigint(21) unsigned | NO | | 0 | |#事务并发票数|
| trx_isolation_level | varchar(16) | NO | | | |#事务隔离级别|
| trx_unique_checks | int(1) | NO | | 0 | |#是否唯一性检查|
| trx_foreign_key_checks | int(1) | NO | | 0 | |#是否外键检查|
| trx_last_foreign_key_error | varchar(256) | YES | | NULL | |#最后的外键错误|
| trx_adaptive_hash_latched | int(1) | NO | | 0 | |#|
| trx_adaptive_hash_timeout | bigint(21) unsigned | NO | | 0 | |#|

```sql
SELECT
     b.trx_state,
     e.state,
     e.time,
     d.state AS block_state,
     d.time AS block_time,
     a.requesting_trx_id,
     a.requested_lock_id,
     b.trx_query,
     b.trx_mysql_thread_id,
     a.blocking_trx_id,
     a.blocking_lock_id,
     c.trx_query AS block_trx_query,
     c.trx_mysql_thread_id AS block_trx_mysql_tread_id
FROM
     information_schema.INNODB_LOCK_WAITS a
LEFT JOIN information_schema.INNODB_TRX b ON a.requesting_trx_id = b.trx_id
LEFT JOIN information_schema.INNODB_TRX c ON a.blocking_trx_id = c.trx_id
LEFT JOIN information_schema.PROCESSLIST d ON c.trx_mysql_thread_id = d.id
LEFT JOIN information_schema.PROCESSLIST e ON b.trx_mysql_thread_id = e.id
ORDER BY
     a.requesting_trx_id;
```

