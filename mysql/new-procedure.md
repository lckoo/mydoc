# 测试的
```sql
DROP PROCEDURE IF EXISTS create_index;
CREATE PROCEDURE create_index(IN tb_name VARCHAR(100), IN idx_name VARCHAR(100), IN idx_column_name VARCHAR(200))
  BEGIN
    DECLARE create_sql VARCHAR(500);
    SET @create_sql = concat(' alter table ', tb_name, ' add index ', idx_name, ' ( ', idx_column_name, ')');
    SELECT count(1)
    INTO @count
    FROM information_schema.statistics
    WHERE table_name = tb_name AND index_name = idx_name;
    IF @count = 0
    THEN
      PREPARE stmt FROM @create_sql;
      EXECUTE stmt;
    END IF;
  END;
```