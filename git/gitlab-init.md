# 命令行指令
## 全局设置
```bash
git config --global user.name "lckoo"
git config --golbal user.email "xiaochaokoo@qq.com"
```

## 创建一个新的仓库
```bash
git clone https://gitlab.com/lckoo/mydoc.git
cd mydoc
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

## 已经存在的文件夹
```bash
cd existing_folder
git init
git remote add origin https://gitlab.com/lckoo/mydoc.git
git add .
git commit -m "Initial commit"
git push -u origin master
```
## 已经存在的本地git仓库
```bash

cd existing_repo
git remote add origin https://gitlab.com/lckoo/mydoc.git
git push -u origin --all
git push -u origin --tags
```