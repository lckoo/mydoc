# 持续集成gitlab-ci

**参考文章**
https://segmentfault.com/a/1190000007180257
https://segmentfault.com/a/1190000009139318
https://www.zybuluo.com/bornkiller/note/314902
https://segmentfault.com/a/1190000006120164
## 名词解释
**Gitlab**
> Gitlab是一个利用Ruby on Rails开发的开源应用程序，实现一个自托管的Git仓库。类似GitHub的功能

**Gitlab-CI**
>即Gitlab Continuous Integration,持续集成。
Gitlab8.0开始，全面集成了Gitlab-ci，并且对所有项目默认开启。
只需要在项目根目录下添加.gitlab-ci.yml文件，并且配置了Runner，那么每次的合并请求或者push都会触发CI pipeline。

**Giblab-Runner**
>Gitlab-Runner是.gitlab-ci.yml的脚本运行器，是一个独立的服务，考虑到Runner运行的资源消耗和安全问题，不建议把Runner和Gitlab安装在一台机器上。

**Badges**
> 徽章，当Pipelines执行完成，会生成徽章

## 安装配置
