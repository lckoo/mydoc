#!/bin/sh
SERVICE_NAME=mydoc
PID_PATH_NAME=/user/local/temp/bear-pid
echo "Starting $SERVICE_NAME ..."
if [ ! -f $PID_PATH_NAME ]; then
    nohup http-server /user/local/public > /dev/null >> /dev/null &
                echo $! > $PID_PATH_NAME
    echo "$SERVICE_NAME started ..."
else
    echo "$SERVICE_NAME is already running ..."
fi
exit;
