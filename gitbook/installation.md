# 安装 gitbook

## 前置条件
- NodeJS (v4.0.0 and above is recommended)
- Windows, Linux, Unix, or Mac OS X

## node
*[安装npm](../nodejs/installation-linux.md)

## 使用npm安装
```bash
npm install gitbook-cli -g
```

## 查看版本
```bash
gitbook -V
```

## 创建book
```bash
gitbook init

```

## 安装book.js中的插件依赖
```bash
gitbook install
```

## 生成静态文件
```
gitbook build
```

## 预览
```bash
gitbook serve
```