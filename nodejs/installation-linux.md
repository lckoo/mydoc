# linux（centos）安装nodejs

## 二进制文件方式

** 下载 **

[https://nodejs.org/en/download/](https://nodejs.org/en/download/)

```bash
cd /usr/local
sudo wget https://nodejs.org/dist/v6.11.2/node-v6.11.2-linux-x64.tar.xz
```

> 说明：压缩包也是打包后再压缩，外面是xz压缩方式，里层是tar打包方式

### 删除旧版本

```bash
#删除原有安装
rm -rf node
rm -rf node-v0.10.29-linux-x64
```

### 解压

```bash
xz -d ***.tar.xz  //先解压xz
tar -xvf  ***.tar //再解压tar
```

## 重命名
```bash
mv node-v6.11.2-linux-x64 node
```

### 设置环境变量

```bash
#添加环境变量并使之生效，内容如下：(当前连接)
export PATH=/usr/local/python/bin:/usr/local/node/bin:$PATH
```

```bash
# 修改系统级别profile
vi /etc/profile
# 添加如下
export PATH=/usr/local/node/bin:$PATH
# 立即生效
source /etc/profile
```

```bash
~/profile 是用户级别
```

### 查看版本

```bash
node -v
```





```bash
#--------------------测试----------------------------
#创建nodejs项目目录
mkdir -p /usr/local/nodejs/

#创建hello.js文件
vi /usr/local/nodejs/hello.js

#内容如下：
var http = require("http");
http.createServer(function(request, response) {
    response.writeHead(200, {
        "Content-Type" : "text/plain" // 输出类型
    });
    response.write("Hello World");// 页面输出
    response.end();
}).listen(8100); // 监听端口号
console.log("nodejs start listen 8102 port!");


#后台运行
node /usr/local/nodejs/hello.js 
&


#浏览器访问
http://192.168.2.2:8100/
```



