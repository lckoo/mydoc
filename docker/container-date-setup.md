# docker容器的日期和宿主机不一致
> 用docker 起了一个mysql服务，发现mysql的sysDate()和宿主机器不一致，差8小时，估计是docker容器的时区和宿主机不一致，因为mysql的日期取得是宿主机的

** 查看主机日期 **
```bash
date
```

** 查看mysql日期、时区 **
```sql
show variables like '%zone%';
SELECT sysdate();
```
## 共享主机的localtime(方法一)
创建容器的时候指定启动参数，挂在localtime文件到容器内
```bash
docker run --name <name> -v /etc/localtime:/etc/localtime:ro

```

## 复制主机的localtime(方法二)

```bash
docker cp /etc/localtime【容器ID或者NAME】:/etc/localtime
```
但是在容器正在运行的程序的日期不会更新，需要重新启动程序或者docker容器

## 创建自定义的Dockerfile(方法三)

```bash
FROM redis

FROM tomcat

ENV CATALINA_HOME /usr/local/tomcat

#设置时区
RUN /bin/cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime \ && echo 'Asia/Shanghai' >/etc/timezone \
```
> 保存后利用docker build命令生成镜像使用即可