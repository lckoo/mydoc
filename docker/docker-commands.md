# docker 常用命令
## 进入容器

```bash
docker attach container_id 
docker attach container_name
```
> 如果连接终止或者使用了exit命令，容器就会退出后台运行

```
docker exec -it container_id  /bin/sh
docker exec -it  container_name /bin/sh
```
